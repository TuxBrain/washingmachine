#include "ConfigHW.h"
#include <TM1638plus.h>
#include "Arduino.h"
#include "uTimerLib.h"

#ifndef LED_BUILTIN
	// change to fit your needs
	// Use 0 or 1 to use DigiSpark AVR internal LED (depending revision, mine is 1)
	#define LED_BUILTIN 13
#endif







// GPIO I/O pins on the Arduino connected to strobe, clock, data,
//pick on any I/O you want.
#define  STROBE_TM 10
#define  CLOCK_TM  9
#define  DIO_TM    8
#define  MT1       11
#define  MT2       12
#define  MT_ON     0
#define  MT_OFF    1



bool high_freq = false; //default false,, If using a high freq CPU > ~100 MHZ set to true. 

//Constructor object (GPIO STB , GPIO CLOCK , GPIO DIO, use high freq MCU default false)
TM1638plus tm(STROBE_TM, CLOCK_TM , DIO_TM, high_freq);



volatile bool status = 0;
uint8_t volatile Botones;



void timed_function() {
static int cont = 50;
  cont--;
  if(cont == 0){
    cont=50;
    status = !status;
    digitalWrite(LED_BUILTIN, status);
  }
  Botones=tm.readButtons();
  //tm.setLED(LEDposition, value & 1);
      /* buttons contains a byte with values of button s8s7s6s5s4s3s2s1
       HEX  :  Switch no : Binary
       0x01 : S1 Pressed  0000 0001
       0x02 : S2 Pressed  0000 0010
       0x04 : S3 Pressed  0000 0100
       0x08 : S4 Pressed  0000 1000
       0x10 : S5 Pressed  0001 0000
       0x20 : S6 Pressed  0010 0000
       0x40 : S7 Pressed  0100 0000
       0x80 : S8 Pressed  1000 0000
      */  

}






void setup() {
  MotorLavar(3);
  tm.displayBegin();
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(MT1, OUTPUT);
  pinMode(MT2, OUTPUT);
  TimerLib.setInterval_us(timed_function, 20000);


}

void loop() {
  tm.displayText("LAvAr ...");
  MotorLavar(0);
  delay(2000);
  MotorLavar(3);
  delay(2000);
  MotorLavar(1);
  delay(2000);
  tm.displayText("SECANDO ");
  MotorLavar(3);
  delay(2000);
  tm.displayText("LLENANDO ..."); 
  delay(2000);
  tm.displayText("TERMINADO"); 
  delay(2500);

}


int MotorLavar(int dat){

    switch(dat){
      case 0:
        digitalWrite(MT1,MT_ON);
        digitalWrite(MT2,MT_OFF);
        Serial.println("MT1=MT_ON");
        Serial.println("MT2=MT_OFF");
        break;
      case 1:
        digitalWrite(MT1,MT_OFF);
        digitalWrite(MT2,MT_ON);
        Serial.println("MT1=MT_OFF");
        Serial.println("MT2=MT_ON");
        break;
      default:
        digitalWrite(MT1,MT_OFF);
        digitalWrite(MT2,MT_OFF);
        Serial.println("MT1 = MT_OFF");
        Serial.println("MT2 = MT_OFF");
        break;
    }
}//END FUNC.












